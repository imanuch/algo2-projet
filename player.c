// add the needed C libraries below
#include <stdbool.h> // bool, true, false
#include <stdlib.h>	 // rand
#include <stdio.h>
// look at the file below for the definition of the direction type
// pacman.h must not be modified!
#include "pacman.h"

//je déclare une nouvelle structure similaire à la structure dans pacman.h mais l'élément contenu dans l'arbre est un entier et non un charactère
//cette structure permettra de faire un arbre parallèle pondéré permettant de choisir un chemin optimal
struct item_node
{
	int item;
	struct item_node *n;
	struct item_node *e;
	struct item_node *s;
	struct item_node *w;
};
typedef struct item_node *item_tree;

// ascii characters used for drawing levels
extern const char PACMAN;	   // ascii used for pacman
extern const char WALL;		   // ascii used for the walls
extern const char PATH;		   // ascii used for the explored paths
extern const char DOOR;		   // ascii used for the ghosts' door
extern const char VIRGIN_PATH; // ascii used for the unexplored paths
extern const char ENERGY;	   // ascii used for the energizers
extern const char GHOST1;	   // ascii used for the ghost 1
extern const char GHOST2;	   // ascii used for the ghost 2
extern const char GHOST3;	   // ascii used for the ghost 3
extern const char GHOST4;	   // ascii used for the ghost 4

// reward (in points) when eating dots/energizers
extern const int VIRGIN_PATH_SCORE; // reward for eating a dot
extern const int ENERGY_SCORE;		// reward for eating an energizer

// put the student names below (mandatory)
const char *binome = "Cardot Emmanuel";

// put the prototypes of your additional functions/procedures below
bool fantome(char item);
bool fantomeproche(itemtree ARBRE);
bool droitelibre(itemtree easttree);
bool gauchelibre(itemtree westtree);
bool hautlibre(itemtree northtree);
bool baslibre(itemtree southtree);
itemtree creerARBRE(char i);
item_tree creerARBREint(int i);
int pondererfantom1(itemtree ARBREchar,item_tree ARBREint, int n);
item_tree remplirARBRE(itemtree ARBREcomplet,bool energy,int remainingenergymoderounds,int n);
int max(int a, int b, int c, int d);
direction maxdir(float N, float S, float W, float E);
int max3(int a, int b, int c);
int calculARBRE(item_tree ARBRE, direction lastdirection);
void retour(float *N, float *S, float *W, float *E, direction lastdirection);
direction second(direction premier, float N, float S, float W, float E);
direction choix_final(float N, float S, float W, float E,dirtree treemap,dirtree treepos,itemtree ARBRE,direction lastdirection,bool energy,int remainingenergymoderounds);
direction oualler(dirtree treemap,dirtree treepos,itemtree northtree,itemtree easttree,itemtree southtree,itemtree westtree,direction lastdirection,bool energy,int remainingenergymoderounds);
// change the pacman function below to build your own player
// your new pacman function can use as many additional functions/procedures as needed; put the code of these functions/procedures *AFTER* the pacman function
direction pacman(
	dirtree treemap,			  // tree of the moves made since the beginning of the game (see pacman.h for the dirtree type); the direction value of the root node is set to -1
	dirtree treepos,			  // points to the dirtree treemap node in which pacman currently stands
	itemtree northtree,			  // tree of the items pacman sees when looking in the north direction (see pacman.h for the itemtree type)
	itemtree easttree,			  // tree of the items pacman sees when looking in the east direction (see pacman.h for the itemtree type)
	itemtree southtree,			  // tree of the items pacman sees when looking in the south direction (see pacman.h for the itemtree type)
	itemtree westtree,			  // tree of the items pacman sees when looking in the west direction (see pacman.h for the itemtree type)
	direction lastdirection,	  // last move made by pacman (see pacman.h for the direction type; lastdirection value is -1 at the beginning of the game
	bool energy,				  // is pacman in energy mode?
	int remainingenergymoderounds // number of remaining rounds in energy mode, if energy mode is true
)
{
	direction d; // the direction to return
	// put your code here

	// answer to the game engine
	d = oualler(
		treemap,
		treepos,
		northtree,
		easttree,
		southtree,
		westtree,
		lastdirection,
		energy,
		remainingenergymoderounds);
	return d;
}

// cette fonction renvoie true si la case choisie est un fantome
bool fantome(char item)
{
	if (item == GHOST1 || item == GHOST2 || item == GHOST3 || item == GHOST4)
	{
		return true;
	}
	return false;
}
// cette fonction renvoie true si un fantome est à une case du pac-man
bool fantomeproche(itemtree ARBRE){
	if (((ARBRE->e)!=NULL && fantome((ARBRE->e)->item)) || ((ARBRE->n)!=NULL && fantome((ARBRE->n)->item)) || ((ARBRE->s)!=NULL && fantome((ARBRE->s)->item)) || ((ARBRE->w)!=NULL && fantome((ARBRE->w)->item))){
		return true;
	}
	return false;
}
//les fonctions suivantes permettent de savoir si il y a ou non un mur ou une porte sur la case choisie
//par exemple : droitelibtre(ARBRE) renvoie true si il n'y a ni porte ni mur à droite
bool droitelibre(itemtree easttree)
{
	if (easttree->item != WALL && easttree->item != DOOR)
	{
		return true;
	}
	return false;
}
bool gauchelibre(itemtree westtree)
{
	if (westtree->item != WALL && westtree->item != DOOR)
	{
		return true;
	}
	return false;
}
bool hautlibre(itemtree northtree)
{
	if (northtree->item != WALL && northtree->item != DOOR)
	{
		return true;
	}
	return false;
}
bool baslibre(itemtree southtree)
{
	if (southtree->item != WALL && southtree->item != DOOR)
	{
		return true;
	}
	return false;
}

//cette fonction permet de créer un arbre selon la structure donnée dans le .h (avec item = char)
itemtree creerARBRE(char i)
{
	itemtree ARBRE = (itemtree)malloc(sizeof(struct itemnode));
	if (ARBRE != NULL)
	{
		ARBRE->item = i;
		ARBRE->n = NULL;
		ARBRE->e = NULL;
		ARBRE->s = NULL;
		ARBRE->w = NULL;
	}
	return ARBRE;
}
//cette fonction permet de créer un arbre selon la structure donnée en haut du ficher (avec item = int)
item_tree creerARBREint(int i)
{
	item_tree ARBRE = (item_tree)malloc(sizeof(struct item_node));
	if (ARBRE != NULL)
	{
		ARBRE->item = i;
		ARBRE->n = NULL;
		ARBRE->e = NULL;
		ARBRE->s = NULL;
		ARBRE->w = NULL;
	}
	return ARBRE;
}

//cette fonction récursive permet de pondérer négativement les cases à côté d'un fantome pour que le pacman s'approche le moins possible.
//elles sont graduellement pondérées, à une case d'un fantome on donne la valeur -1000, à 2 -750, à 3 -500... 
int pondererfantom1(itemtree ARBREchar,item_tree ARBREint, int n){
	int N,E,W,S;
	if (ARBREchar!=NULL){
		if (fantome(ARBREchar->item)){
			
			return 4;
		}
		N = pondererfantom1(ARBREchar->n,ARBREint->n,n);
		E = pondererfantom1(ARBREchar->e,ARBREint->e,n);
		S = pondererfantom1(ARBREchar->s,ARBREint->s,n);
		W = pondererfantom1(ARBREchar->w,ARBREint->w,n);
		if ((N+S+E+W)>1){
			ARBREint->item -= (N+S+E+W)*250;
			return N+S+E+W-1;
		}
	}
		

	return 0;
}

//cette fonction permet de remplir récursivement l'arbre parallèle contenant des entiers
item_tree remplirARBRE(
	itemtree ARBREcomplet,
	bool energy,
	int remainingenergymoderounds,
	int n)
{

	if (ARBREcomplet != NULL)
	{
		//cette partie est la partie remplissage de la case selon différents critères
		item_tree ARBRE = creerARBREint(0);

		if (ARBREcomplet->item == WALL)
		{
			ARBRE->item = -10000;
		}
		if (fantomeproche(ARBREcomplet) && ARBREcomplet->item==ENERGY){
			ARBRE->item +=300;
		}
		if (ARBREcomplet->item == VIRGIN_PATH)
		{
			ARBRE->item += 150;
		}

		if (fantome(ARBREcomplet->item) && energy && remainingenergymoderounds > n)
		{
			ARBRE->item = +10000;
		}

		if (fantome(ARBREcomplet->item) && (!energy || remainingenergymoderounds<n ))
		{
			ARBRE->item = -10000;
		}
		
		//cette partie est la partie récursive de la fonction
		if (ARBREcomplet->n != NULL)
		{
			ARBRE->n = remplirARBRE(ARBREcomplet->n, energy, remainingenergymoderounds, n + 1);
		}
		if (ARBREcomplet->s != NULL)
		{
			ARBRE->s = remplirARBRE(ARBREcomplet->s, energy, remainingenergymoderounds, n + 1);
		}
		if (ARBREcomplet->e != NULL)
		{
			ARBRE->e = remplirARBRE(ARBREcomplet->e, energy, remainingenergymoderounds, n + 1);
		}
		if (ARBREcomplet->w != NULL)
		{
			ARBRE->w = remplirARBRE(ARBREcomplet->w, energy, remainingenergymoderounds, n + 1);
		}
		return ARBRE;
	}
	return NULL;
}
//renvoie le maximum entre 4 entiers
int max(int a, int b, int c, int d)
{
	if (a >= b && a >= c && a >= d)
	{
		return a;
	}
	else if (b >= c && b >= a && b >= d)
	{
		return b;
	}
	else if (c >= a && c >= b && c >= d)
	{
		return c;
	}
	else
	{
		return d;
	}
}
// retourne la destination ayant le chemin le plus intéressant
direction maxdir(float N, float S, float W, float E)
{
	if (N >= S && N >= W && N >= E)
	{
		return NORTH;
	}
	else if (S >= N && S >= W && S >= E)
	{
		return SOUTH;
	}
	else if (W >= N && W >= S && W >= E)
	{
		return WEST;
	}
	else
	{
		return EAST;
	}
}
// retourne le nombre maximum entre 3 nombres
int max3(int a, int b, int c)
{
	if (a >= b && a >= c)
	{
		return a;
	}
	else if (b >= c && b >= a)
	{
		return b;
	}
	else if (c >= a && c >= b)
	{
		return c;
	}
	else
	{
		return a;
	}
}

// retourne la somme de tous les entiers du chemin le plus rentable
int calculARBRE(item_tree ARBRE, direction lastdirection)
{	int n = 150;
	if (ARBRE==NULL){
		return 0;
	}
	//if (ARBRE->e != NULL && ARBRE->n != NULL && ARBRE->s != NULL && ARBRE->w != NULL)
	else 
	{
		if (lastdirection == NORTH)
		{
			return ARBRE->item + max(calculARBRE(ARBRE->n, NORTH), calculARBRE(ARBRE->s, SOUTH) - n, calculARBRE(ARBRE->w, WEST), calculARBRE(ARBRE->e, EAST));
		}
		else if (lastdirection == SOUTH)
		{
			return ARBRE->item + max(calculARBRE(ARBRE->n, NORTH) - n, calculARBRE(ARBRE->s, SOUTH), calculARBRE(ARBRE->w, WEST), calculARBRE(ARBRE->e, EAST));
		}
		else if (lastdirection == WEST)
		{
			return ARBRE->item + max(calculARBRE(ARBRE->n, NORTH), calculARBRE(ARBRE->s, SOUTH), calculARBRE(ARBRE->w, WEST), calculARBRE(ARBRE->e, EAST) - n);
		}
		else if (lastdirection == EAST)
		{
			return ARBRE->item + max(calculARBRE(ARBRE->n, NORTH), calculARBRE(ARBRE->s, SOUTH), calculARBRE(ARBRE->w, WEST) - n, calculARBRE(ARBRE->e, EAST));
		}
	}
	return -1000000;
}
// permet de pénaliser le retour en arrière
void retour(float *N, float *S, float *W, float *E, direction lastdirection)
{
	int k = 500;
	if (lastdirection == NORTH)
	{
		*S -= k;
	}
	else if (lastdirection == SOUTH)
	{
		*N -= k;
	}
	else if (lastdirection == EAST)
	{
		*W -= k;
	}
	else if (lastdirection == WEST)
	{
		*E -= k;
	}
}
// permet de connaitre le deuxième ininéraire le plus rentable si le premier est inaccessible
direction second(direction premier, float N, float S, float W, float E)
{
	if (premier == NORTH)
	{
		return maxdir(-1000000000, S, W, E);
	}
	else if (premier == SOUTH)
	{
		return maxdir(N, -1000000000, W, E);
	}
	else if (premier == WEST)
	{
		return maxdir(N, S, -1000000000, E);
	}
	else if (premier == EAST)
	{
		return maxdir(N, S, W, -1000000000);
	}
	else
	{
		return WEST;
	}
}

//cette fonction est la dernière appelée par oualler(), elle permet de donner la dernière décision concernant le chemin à prendre
direction choix_final(float N, float S, float W, float E,
					  dirtree treemap,
					  dirtree treepos,
					  itemtree ARBRE,
					  direction lastdirection,
					  bool energy,
					  int remainingenergymoderounds)
{
	// Si par mégarde un chemin choisi mène directement sur un mur(normalement impossible), on propose une alternative avec second()
	direction d = WEST;
	direction prems = maxdir(N, S, W, E);

	if (prems == NORTH && hautlibre(ARBRE->n))
	{
		d = NORTH;
		return d;
	}

	else if (prems == SOUTH && baslibre(ARBRE->s))
	{
		d = SOUTH;
		return d;
	}

	else if (prems == WEST && gauchelibre(ARBRE->w))
	{
		d = WEST;
		return d;
	}

	else if (prems == EAST && droitelibre(ARBRE->e))
	{
		d = EAST;
		return d;
	}

	else if (second(prems, N, S, W, E) == NORTH && hautlibre(ARBRE->n))
	{
		d = NORTH;
		return d;
	}

	else if (second(prems, N, S, W, E) == SOUTH && baslibre(ARBRE->s))
	{
		d = SOUTH;
		return d;
	}
	else if (second(prems, N, S, W, E) == WEST && gauchelibre(ARBRE->w))
	{
		d = WEST;
		return d;
	}

	else if (second(prems, N, S, W, E) == EAST && droitelibre(ARBRE->e))
	{
		d = EAST;
		return d;
	}

	else if (droitelibre((ARBRE->e)) == true && fantome((ARBRE->e)->item))
	{
		return EAST;
	}
	else if (gauchelibre((ARBRE->w)) == true && fantome((ARBRE->w)->item))
	{
		return WEST;
	}
	else if (hautlibre((ARBRE->n)) == true && fantome((ARBRE->n)->item))
	{
		return NORTH;
	}
	else if (baslibre((ARBRE->s)) == true && fantome((ARBRE->s)->item))
	{
		return SOUTH;
	}
	else
	{
		return WEST;
	}

	return d;
}

//cette fonction peut être considérer comme étant le main
direction oualler(
	dirtree treemap,
	dirtree treepos,
	itemtree northtree,
	itemtree easttree,
	itemtree southtree,
	itemtree westtree,
	direction lastdirection,
	bool energy,
	int remainingenergymoderounds)
{
	//cet arbre est l'arbre parallèle contenant des chiffres
	item_tree ARBRE = creerARBREint(0);
	//4 flottants contenant les valeurs max de chaque chemin
	float N, S, W, E;
	//cette arbre est une fusion des 4 arbres donnés
	itemtree ARBREcomplet = creerARBRE('0');
	ARBREcomplet->n = northtree;
	ARBREcomplet->e = easttree;
	ARBREcomplet->s = southtree;
	ARBREcomplet->w = westtree;

	ARBRE = remplirARBRE(ARBREcomplet, energy, remainingenergymoderounds, 0);
	if (!energy){
		pondererfantom1(ARBREcomplet,ARBRE,0);
	}
	N = calculARBRE(ARBRE->n, lastdirection);
	S = calculARBRE(ARBRE->s, lastdirection);
	W = calculARBRE(ARBRE->w, lastdirection);
	E = calculARBRE(ARBRE->e, lastdirection);
	retour(&N, &S, &W, &E, lastdirection);
	return choix_final(N, S, W, E, treemap, treepos, ARBREcomplet, lastdirection, energy, remainingenergymoderounds);
}