// add the needed C libraries below
#include <stdbool.h> // bool, true, false
#include <stdlib.h> // rand
#include <stdio.h>
#include <math.h>
// look at the file below for the definition of the direction type
// pacman.h must not be modified!
#include "pacman.h"

struct item_node { // piece of Snake tree-shaped vision 
  int item; // map item
  struct item_node * pnn; // further item towards north 
  struct item_node * pen; // further item towards east
  struct item_node * psn; // further item towards south
  struct item_node * pwn; // further item towards west
};
typedef struct item_node * item_tree; // Snake tree-shaped vision

// ascii characters used for drawing levels
extern const char PACMAN; // ascii used for pacman
extern const char WALL; // ascii used for the walls
extern const char PATH; // ascii used for the explored paths
extern const char DOOR; // ascii used for the ghosts' door
extern const char VIRGIN_PATH; // ascii used for the unexplored paths
extern const char ENERGY; // ascii used for the energizers
extern const char GHOST1; // ascii used for the ghost 1
extern const char GHOST2; // ascii used for the ghost 2
extern const char GHOST3; // ascii used for the ghost 3
extern const char GHOST4; // ascii used for the ghost 4int

// reward (in points) when eating dots/energizers 
extern const int VIRGIN_PATH_SCORE; // reward for eating a dot
extern const int ENERGY_SCORE; // reward for eating an energizer

// put the student names below (mandatory)
const char * binome="Cardot Emmanuel";

// put the prototypes of your additional functions/procedures below
bool fantoupas(char * * map,int x, int y);
bool droitelibre(char * * map,int x, int y);
bool gauchelibre(char * * map,int x, int y,int xsize);
bool hautlibre(char * * map,int x, int y);
bool baslibre(char * * map,int x, int y,int ysize);
int distf(char * * map,int x, int y,int xsize, int ysize);
item_tree creerGRAPH(int i);
int modabs(int a, int b);
void remplirGRAPH(int x, int y,int n,int xsize,int ysize, float mapss2[ysize][xsize],item_tree GRAPH);
void map2(char * * map, int xsize, int ysize, float maps[ysize][xsize],int x, int y,direction lastdirection, bool energy, int remainingenergymoderounds, int n);
int max(int a, int b, int c,int d);
void retour(float* N,float* S,float* W,float* E,direction lastdirection);
int calculGRAPH(item_tree GRAPH,direction lastdirection);
double dist(int x, int y,int i,int j);
direction maxdir(float N,float S,float W,float E);
int max3(int a,int b,int c);
direction second(direction premier,float N,float S,float W,float E);
direction choix_final(float N,float S,float W,float E,char ** map,int x,int y,int ysize,int xsize);
direction oualler(char * * map,int xsize,int ysize,int x,int y,direction lastdirection,bool energy,int remainingenergymoderounds );

// change the pacman function below to build your own player
// your new pacman function can use as many additional functions/procedures as needed; put the code of these functions/procedures *AFTER* the pacman function
direction pacman(
		 char * * map, // the map as a dynamic array of strings, ie of arrays of chars
		 int xsize, // number of columns of the map
		 int ysize, // number of lines of the map
		 int x, // x-position of pacman in the map 
		 int y, // y-position of pacman in the map
		 direction lastdirection, // last move made by pacman (see pacman.h for the direction type; lastdirection value is -1 at the beginning of the game
		 bool energy, // is pacman in energy mode? 
		 int remainingenergymoderounds // number of remaining rounds in energy mode, if energy mode is true
		 ) {
  direction d; // the direction to returnbool hautlibre(char * * map,int x, int y,int xsize)
	

	d = oualler(map,xsize,ysize,x,y,lastdirection,energy,remainingenergymoderounds); 
  
  return d; // answer to the game engine
}
//return false si il y a un fantome à la case x,y
bool fantoupas(char * * map,int x, int y){
	if (map[y][x] == GHOST1 || map[y][x] == GHOST2 || map[y][x] == GHOST3 || map[y][x] == GHOST4) {return false;}
	else {
		return true;
	};
}
//les 4prochaines fonctions permettent de renseigner sur la présence de mur ou non sur chaque case contigue au personnage
bool droitelibre(char * * map,int x, int y){
	if((x==0 || (x>0 && map[y][x+1]!=WALL && map[y][x+1]!=DOOR)) ) {return true;}
	return false;
}
bool gauchelibre(char * * map,int x, int y,int xsize){
	if(x==xsize-1 || (x<xsize-1 && map[y][x-1]!=WALL && map[y][x-1]!=DOOR )) {return true;}
	return false;
}
bool hautlibre(char * * map,int x, int y){
	if(y==0 || (y>0 && map[y-1][x]!=WALL && map[y-1][x]!=DOOR )) {return true;}
	return false;
}
bool baslibre(char * * map,int x, int y,int ysize){
	if(y==ysize-1 || (y<ysize-1 && map[y+1][x]!=WALL && map[y+1][x]!=DOOR )) {return true;}
	return false;
}



//retourne la distance entre une case et un fantôme
int distf(char * * map,int x, int y,int xsize, int ysize){
	int i;
	int j;
	double dist = 1000;
	double k;
	for (i=0;i<xsize;i++){
		for (j=0;j<ysize;j++){
			if (!fantoupas(map,i,j)){
				k = sqrt((i-x)*(i-x)+(j-y)*(j-y));
				if (k<dist){
					dist = k;
				}
			}
		}
	}
  return dist;
}
//retourne un graph initialisé
item_tree creerGRAPH(int i){
	item_tree GRAPH = (item_tree) malloc(sizeof(struct item_node));
	if (GRAPH!=NULL){
		GRAPH->item=i;
		GRAPH->pen = NULL;
		GRAPH->pwn = NULL;
		GRAPH->psn = NULL;
		GRAPH->pnn = NULL;
	}
	return GRAPH;
}

//utilise le modulo type python pour les éléments négatifs
int modabs(int a, int b){
	if (a<0){return a+b;}
	return a;
}
//rempli un graphe à partir des éléments de la carte
void remplirGRAPH(int x, int y,int n,int xsize,int ysize, float mapss2[ysize][xsize],item_tree GRAPH){
  
  if (x>= xsize || y>=ysize){
	GRAPH->item = 0;
  }
  GRAPH->item = mapss2[y][x];
  if (mapss2[y][x]>1000000){
	
  }

  if (n-1>0){
	GRAPH->pen = creerGRAPH(0);
	GRAPH->pwn = creerGRAPH(0);
	GRAPH->psn = creerGRAPH(0);
	GRAPH->pnn = creerGRAPH(0);
    remplirGRAPH((x+1)%(xsize-1),y,n-1,xsize,ysize,mapss2,GRAPH->pen);
    remplirGRAPH(modabs(x-1,xsize-1),y,n-1,xsize,ysize,mapss2,GRAPH->pwn);
    remplirGRAPH(x,modabs(y-1,ysize-1),n-1,xsize,ysize,mapss2,GRAPH->pnn);
    remplirGRAPH(x,(y+1)%(ysize-1),n-1,xsize,ysize,mapss2,GRAPH->psn);
  }
}

//pondère une map préalablement initialisée 
void map2(
	char * * map, // the map as a dynamic array of strings, ie of arrays of chars
		 int xsize, // number of columns of the map
		 int ysize, // number of lines of the map
		 float maps[ysize][xsize],
		 int x, // x-position of pacman in the map 
		 int y, // y-position of pacman in the map
		 direction lastdirection, // last move made by pacman (see pacman.h for the direction type; lastdirection value is -1 at the beginning of the game
		 bool energy, // is pacman in energy mode? 
		 int remainingenergymoderounds, // number of remaining rounds in energy mode, if energy mode is true
		 int n
		 ){
      int i;
      int j;
      
      for (i=0;i<xsize;i++){
        for (j=0;j<ysize;j++){

          maps[j][i] = -1;

          if (map[j][i]== WALL || map[j][i]==DOOR){
            maps[j][i]=-1000000;
			}
          if (map[j][i]==VIRGIN_PATH){
            maps[j][i]=150;

          }

          if (!fantoupas(map,i,j) && energy && remainingenergymoderounds>dist(x,y,i,j)){
            maps[j][i]+=10000;
          }
          else if (!fantoupas(map,i,j) && !energy){
            maps[j][i]=-10000;
          }

          if (distf(map,i,j,xsize,ysize)<3){
			
            maps[j][i]-= (3-distf(map,i,j,xsize,ysize))*1500;
          }

          }
        }

        for (i=0;i<xsize;i++){
          for (j=0;j<ysize;j++){
            maps[j][i] = maps[j][i]+dist(x,y,i,j)*(-0.5);
      }}
}
//retourne le maximum entre 4 entiers
int max(int a, int b, int c,int d){
  if (a>=b && a>=c && a>=d){return a;}
  else if (b>=c && b>=a && b>=d){return b;}
  else if (c>=a && c>=b && c>=d){return c;}
  else {return d;}
}
//permet de pénaliser le retour en arrière
void retour(float* N,float* S,float* W,float* E,direction lastdirection){
	int k = 1000;
	if (lastdirection==NORTH){*S-=k;}
	else if (lastdirection==SOUTH){*N-=k;}
	else if (lastdirection==EAST){*W-=k;}
	else if (lastdirection==WEST){*E-=k;}
}
//retourne la somme de tous les entiers du chemin le plus rentable
int calculGRAPH(item_tree GRAPH,direction lastdirection){
	int n = 150;
	if (GRAPH->pen !=NULL && GRAPH->pnn !=NULL && GRAPH->psn !=NULL && GRAPH->pwn !=NULL){
		if (lastdirection==NORTH){
  			return GRAPH->item + max(calculGRAPH(GRAPH->pnn,NORTH),calculGRAPH(GRAPH->psn,SOUTH)-n,calculGRAPH(GRAPH->pwn,WEST),calculGRAPH(GRAPH->pen,EAST));
		}
		else if (lastdirection==SOUTH){
  			return GRAPH->item + max(calculGRAPH(GRAPH->pnn,NORTH)-n,calculGRAPH(GRAPH->psn,SOUTH),calculGRAPH(GRAPH->pwn,WEST),calculGRAPH(GRAPH->pen,EAST));
		}
		else if (lastdirection==WEST){
  			return GRAPH->item + max(calculGRAPH(GRAPH->pnn,NORTH),calculGRAPH(GRAPH->psn,SOUTH),calculGRAPH(GRAPH->pwn,WEST),calculGRAPH(GRAPH->pen,EAST)-n);
		}
		else if (lastdirection==EAST){
  			return GRAPH->item + max(calculGRAPH(GRAPH->pnn,NORTH),calculGRAPH(GRAPH->psn,SOUTH),calculGRAPH(GRAPH->pwn,WEST)-n,calculGRAPH(GRAPH->pen,EAST));
		}
		}
	return 0;
}
//retourne la distance entre 2 éléments de la carte
double dist(int x, int y,int i,int j){
	return sqrt(((i-x)*(i-x)+(j-y)*(j-y)));
}
//retourne la destination ayant le chemin le plus intéressant 
direction maxdir(float N,float S,float W,float E){
	  if (N>=S && N>=W && N>=E){return NORTH;}
  else if (S>=N && S>=W && S>=E){return SOUTH;}
  else if (W>=N && W>=S && W>=E){return WEST;}
  else {return EAST;}
}
// retourne le nombre maximum entre 3 nombres
int max3(int a,int b,int c){
	if (a>=b && a>=c ){return a;}
  	else if (b>=c && b>=a ){return b;}
  	else if (c>=a && c>=b ){return c;}
	else {return a;}
}
//permet de connaitre le deuxième ininéraire le plus rentable si le premier est inaccessible 
direction second(direction premier,float N,float S,float W,float E){
	if (premier == NORTH){return maxdir(-1000000000,S,W,E);}
	else if (premier == SOUTH){return maxdir(N,-1000000000,W,E);}
	else if (premier == WEST){return maxdir(N,S,-1000000000,E);}
	else if (premier == EAST){return maxdir(N,S,W,-1000000000);}
	else {return WEST;}
}
//retourne le choix final de la destination à prendre
direction choix_final(float N,float S,float W,float E,char ** map,int x,int y,int ysize,int xsize){
	direction d = WEST;
	direction prems = maxdir(N,S,W,E);

	
		if(prems==NORTH && hautlibre(map,x,y)){
			d = NORTH;
			return d;
			
		}
		else if (prems==SOUTH && baslibre(map,x,y,ysize)){
			d = SOUTH;
			return d;
			
		}
		else if (prems==WEST && gauchelibre(map,x,y,xsize)){
			d = WEST;
			return d;
			
		}
		else if (prems==EAST && droitelibre(map,x,y)){
			d = EAST;
			return d;
			
		}
		else if(second(prems,N,S,W,E)==NORTH && hautlibre(map,x,y)){
			d = NORTH;
			return d;
			
		}
		else if (second(prems,N,S,W,E)==SOUTH && baslibre(map,x,y,ysize)){
			d = SOUTH;
			return d;
			
		}
		else if (second(prems,N,S,W,E)==WEST && gauchelibre(map,x,y,xsize)){
			d = WEST;
			return d;
			
		}
		else if (second(prems,N,S,W,E)==EAST && droitelibre(map,x,y)){
			d = EAST;
			return d;
			
		}
//si rien n'est possible on fait au mieux avec les fonctions suivantes
		else if (droitelibre(map,x,y)== true && fantoupas(map,x+1,y)){return EAST;}
	else if(gauchelibre(map,x,y,xsize)== true && fantoupas(map,x-1,y)){return WEST;}
	else if(hautlibre(map,x,y) == true && fantoupas(map,x,y-1)){return NORTH;}
	else if(baslibre(map,x,y,ysize) == true && fantoupas(map,x,y+1)){return SOUTH;}
	else {return WEST;}

		return d;
}
//toutes les initialisations sont faites dans cette fonction
direction oualler(
		char * * map, // the map as a dynamic array of strings, ie of arrays of chars
		 int xsize, // number of colum,xsizens of the map
		 int ysize, // number of lines of the map
		 int x, // x-position of pacman in the map 
		 int y, // y-position of pacman in the map
		 direction lastdirection, // last move made by pacman (see pacman.h for the direction type; lastdirection value is -1 at the beginning of the game
		 bool energy, // is pacman in energy mode? 
		 int remainingenergymoderounds // number of remaining rounds in energy mode, if energy mode is true
		 ) {
		
		item_tree GRAPH = (item_tree) malloc(sizeof(struct item_node));
		int i,j;
		float maps2[ysize][xsize];
		for (i=0;i<xsize;i++){
			for (j=0;j<ysize;j++){
				maps2[j][i]=0;
			}
		}


		float N,S,W,E;
		int n = 10;
		GRAPH->item = -1;



		map2(map,xsize,ysize,maps2,x,y,lastdirection,energy,remainingenergymoderounds,n);

		remplirGRAPH(x,y,n,xsize,ysize,maps2,GRAPH);
		
		maps2[y][x]=2600;
		
		N = calculGRAPH(GRAPH->pnn,lastdirection);
		S = calculGRAPH(GRAPH->psn,lastdirection);
		W = calculGRAPH(GRAPH->pwn,lastdirection);
		E = calculGRAPH(GRAPH->pen,lastdirection);
		
		retour(&N,&S,&W,&E,lastdirection);
        free(GRAPH);
		return choix_final(N,S,W,E,map,x,y,ysize,xsize);
	}